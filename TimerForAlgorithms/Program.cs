﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimerForAlgorithms
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Diagnostics.Stopwatch watch1 = System.Diagnostics.Stopwatch.StartNew();
            // the code that you want to measure comes here

            // try 10,000 and 20,000 and 30,000 and 100,000 
            // (100,000 is abour 35 seconds for a loop inside of a loop
            int someLargeNumber = 100000;
            Console.WriteLine("For " + someLargeNumber + " times throught the loop:");

            for (int i = 0; i < someLargeNumber; i++)
            {
                //do some work - it doesn't really mater what
                double value1 = 5.4;
                value1 = value1 / 4.5;
            }

            watch1.Stop();
            long elapsedMs = watch1.ElapsedMilliseconds;
            Console.WriteLine("Linear time: about " + elapsedMs + " milliseconds");


            System.Diagnostics.Stopwatch watch2 = System.Diagnostics.Stopwatch.StartNew();
            // the code that you want to measure comes here

            for (int i = 0; i < someLargeNumber; i++)
            {
                for (int j = 0; j < someLargeNumber; j++)
                {
                    //do some work - it doesn't really mater what
                    double value1 = 5.4;
                    value1 = value1 / 4.5;
                }

            }

            watch2.Stop();
            elapsedMs = watch2.ElapsedMilliseconds;
            Console.WriteLine("Squared time: about " + elapsedMs + " milliseconds");
            Console.ReadLine();

        }
    }
}
